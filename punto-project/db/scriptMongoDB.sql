--- Créer DB PUNTO ---
use punto;

--- Créer collection joueurs ---
db.createCollection("joueurs");

--- Créer collection parties ---
db.createCollection("parties");

--- Créer index unique sur pour joueurs ---
db.joueurs.createIndex({ "id": 1 }, { unique: true, name: "unique_id_index" });

--- Insérer joueurs ---
db.joueurs.insertMany([
  {
    id: 1,
    nom: 'Joueur 1',
    score: 100,
    date_inscription: new Date()
  },
  {
    id: 2,
    nom: 'Joueur 2',
    score: 150,
    date_inscription: new Date()
  }
]);


--- Insérer parties ---
db.parties.insertMany([
  {
    id_joueur1: 1,
    id_joueur2: 2,
    resultat: 'Victoire de Joueur 1',
    date_partie: new Date()
  },
  {
    id_joueur1: 2,
    id_joueur2: 1,
    resultat: 'Victoire de Joueur 2',
    date_partie: new Date()
  }
]);
