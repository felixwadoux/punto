-- Créer une base de données pour le jeu de Punto
CREATE DATABASE punto;

-- Utiliser la base de données
USE punto;

-- Créer une table pour stocker les informations des joueurs
CREATE TABLE joueurs (
    id INT AUTO_INCREMENT PRIMARY KEY,
    nom VARCHAR(255) NOT NULL,
    score INT DEFAULT 0,
    date_inscription TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

-- Créer une table pour stocker les parties jouées
CREATE TABLE parties (
    id INT AUTO_INCREMENT PRIMARY KEY,
    id_joueur1 INT,
    id_joueur2 INT,
    resultat VARCHAR(255),
    date_partie TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (id_joueur1) REFERENCES joueurs(id),
    FOREIGN KEY (id_joueur2) REFERENCES joueurs(id)
);


-- Insérer quelques données de test
-- INSERT INTO joueurs (nom, score) VALUES
-- ('Joueur 1', 100),
-- ('Joueur 2', 150);

-- INSERT INTO parties (id_joueur1, id_joueur2, resultat) VALUES
-- (1, 2, 'Victoire de Joueur 1'),
-- (2, 1, 'Victoire de Joueur 2');
