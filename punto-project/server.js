const express = require('express');
const app = express();
const cors = require('cors');
const mysql = require('mysql2');
const sqlite3 = require('sqlite3');
const mongoose = require('mongoose');
const util = require('util');
const neo4j = require('neo4j-driver');

const port = 4201; // Port utilisé

console.log('Setting up CORS...');
app.use(cors({
    origin: 'http://localhost:8080',
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
    credentials: true,
    optionsSuccessStatus: 204,
  })); 
// Middleware pour gérer les requêtes JSON
app.use(express.json());


// Routes
app.get('/mysql/players', (req, res) => {
  mysqlConnection.query('SELECT * FROM joueurs', (err, results) => {
    if (err) {
      console.error('Erreur lors de la requête MySQL:', err);
      res.status(500).json({ error: 'Erreur lors de la requête MySQL' });
    } else {
      res.json({ message: 'Hello from the API!', data: results });
    }
  });
});

app.get('/sqlite/players', (req, res) => {
  sqliteConnection.all('SELECT * FROM joueurs', (err, results) => {
    if (err) {
      console.error('Erreur lors de la requête SQLite:', err);
      res.status(500).json({ error: 'Erreur lors de la requête SQLite' });
    } else {
      res.json({ message: 'Hello from the API!', data: results });
    }
  });
});


app.get('/mongodb/players', async (req, res) => {
  try {
    const joueurs = mongoose.model('Joueur', mongoose.Schema({}), 'Joueur');
    const results = await joueurs.find({}).exec();

    res.json({ message: 'Hello from the API!', data: results });
  } catch (err) {
    console.error('Erreur lors de la requête MongoDB:', err);
    res.status(500).json({ error: 'Erreur lors de la requête MongoDB' });
  }
});

app.post('/mongodb/generation_parties', async (req, res) => {
  const { numParties, numJoueurs } = req.body;
  try {
    await genererParties(numParties, numJoueurs);
    res.status(200).send('Parties Mongo générées avec succès.');

  } catch (err) {
    console.error("Erreur lors de la génération des parties Mongo:", err);
    res.status(500).send("Une erreur est survenue lors de la génération des parties Mongo.");
  }
});


const mysqlConnection = mysql.createConnection({
  host: '127.0.0.1',
  user: 'root',
  password: 'root',
  database: 'punto'
});

const connectAsync = util.promisify(mysqlConnection.connect).bind(mysqlConnection);
const queryAsync = util.promisify(mysqlConnection.query).bind(mysqlConnection);
const endAsync = util.promisify(mysqlConnection.end).bind(mysqlConnection);

const sqliteConnection = new sqlite3.Database('./db/punto.db');


const main = async () => {
  try {
    // Connexion à MySQL

    connectAsync((err) => {
      if (err) {
        console.error('Erreur de connexion à MySQL:', err);
      } else {
        console.log('Connecté à MySQL');
      }
    });

    // Connexion à SQLite
    sqliteConnection.on('open', () => {
      console.log('Connecté à la base SQLite');
    });   

    // // Connexion à MongoDB
    // mongoose.connect('mongodb://127.0.0.1:27017/punto', { useNewUrlParser: true, useUnifiedTopology: true });

    // const mongodbConnection = mongoose.connection;

    // mongodbConnection.on('error', console.error.bind(console, 'Erreur de connexion à MongoDB:'));
    // mongodbConnection.once('open', () => {
    //   console.log('Connecté à MongoDB');
    // });


    const driver = neo4j.driver('bolt://127.0.0.1:7687/PuntoDB/dbpunto', neo4j.auth.basic('neo4j', '12345678'));
    const session = driver.session();

    session.run('MATCH (n) RETURN n LIMIT 1')
    .then(result => {
      console.log('Connecté à Neo4j');
  
      if (result.records.length > 0) {
        const firstNode = result.records[0].get('n');
        const properties = firstNode.properties;
        const dateInscription = new Date(properties.date_inscription.low * 1000);

        // Afficher les propriétés mises à jour
        console.log("Noeud 1 : ", {
          score: properties.score.low,
          date_inscription: dateInscription,
          id: properties.id.low,
          nom: properties.nom
        });
      } else {
        console.log('Aucun nœud trouvé dans la base de données.');
      }
    })
    .catch(error => {
      console.error('Erreur lors de la connexion ou de l\'exécution de la requête Cypher :', error);
    })
      // .finally(() => {
      //   // Close the session and driver
      //   session.close();
      //   driver.close();
      // });

    
  } catch (error) {
    console.error('Erreur:', error);
    res.status(500).send('Une erreur s\'est produite lors de la connexion à Neo4j.\nRééssayez.');
  }
};

main();



// Définir le schéma pour le modèle Joueur
const joueurSchema = new mongoose.Schema({
  nom: String,
  nbVictoire: Number,
  nbDefaite: Number,
  nbPartie: Number,
});

// Définir le modèle Joueur
const Joueur = mongoose.model('joueurs', joueurSchema);

// Définir le schéma pour le modèle Partie
const partieSchema = new mongoose.Schema({
  idGagnant: mongoose.Schema.Types.ObjectId,
  id_perdant : mongoose.Schema.Types.ObjectId,
  resultat: String,
  date_partie: Date,
});

// Définir le modèle Partie
const PartieModel = mongoose.model('Partie', partieSchema);

async function genererParties(numParties, numJoueurs) {
  try {
    const db = mongoose.connection;
    
    const partiesCollection = db.collection('Partie');
    const joueursCollection = db.collection('Joueur');

    const joueursExistants = await joueursCollection.countDocuments();
    const joueursManquants = Math.max(numJoueurs - joueursExistants, 0);

    if (joueursManquants > 0) {
      const nouveauxJoueurs = [];
      for (let i = 1; i < joueursManquants + 1; i++) {
        const nouveauJoueur = {
          pseudo: `Joueur_${joueursExistants + i}`,
          nbVictoire: 0,
          nbDefaite: 0,
          nbPartie: 0,
        };
        const result = await joueursCollection.insertOne(nouveauJoueur);
        nouveauxJoueurs.push(result.insertedId);
      }
      console.log(`${joueursManquants} nouveaux joueurs créés.`);
    }

    const joueurs = await joueursCollection.find().toArray();
    const joueursIds = joueurs.map(joueur => joueur._id);

    const partiesGeneres = [];
    for (let i = 0; i < numParties; i++) {
      const partie = {
        idGagnant: null,
        Joueurs: [],
        date: new Date(),
        heure: new Date().toLocaleTimeString(),
      };

      const joueursDansPartie = [];
      while (joueursDansPartie.length < numJoueurs) {
        const joueurIndex = Math.floor(Math.random() * joueursIds.length);
        const joueurId = joueursIds[joueurIndex];
        if (!joueursDansPartie.includes(joueurId)) {
          joueursDansPartie.push(joueurId);
        }
      }

      partie.Joueurs = joueursDansPartie;

      partie.idGagnant = partie.Joueurs[Math.floor(Math.random() * numJoueurs)];

      await joueursCollection.updateMany(
        { _id: { $in: partie.Joueurs } },
        { $inc: { nbPartie: 1 } }
      );

      await joueursCollection.updateOne(
          { _id: partie.idGagnant },
          { $inc: { nbVictoire: 1 } }
      );

      await joueursCollection.updateMany(
          { _id: { $in: partie.Joueurs, $ne: partie.idGagnant } },
          { $inc: { nbDefaite: 1 } }
      );

      // Utilisation de Mongoose pour insérer la partie
      try {
        const result = await PartieModel.create(partie);
        partiesGeneres.push(result._id);
      } catch (error) {
        console.error('Erreur lors de l\'insertion de la partie dans MongoDB :', error);
      }
    }

    console.log(`Parties générées avec succès: ${partiesGeneres}`);

  } catch (error) {
    console.error('Erreur lors de la génération des parties :', error);
  }
  
}





// Lancer le serveur
app.listen(port, () => {
  console.log(`Serveur en cours d'exécution sur le port ${port}`);
});
