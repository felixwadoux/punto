import { createRouter, createWebHistory } from 'vue-router'
import Punto from "@/views/Punto.vue";
import ChoixBDD from "@/views/ChoixBDD.vue";
import DatabaseService from '../services/DatabaseService';


const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'choixBDD',
      component: ChoixBDD
    },
    {
      path: "/punto",
      name: "punto",
      component: Punto,
    },
    // {
    //   path: '/about',
    //   name: 'about',
    //   // route level code-splitting
    //   // this generates a separate chunk (About.[hash].js) for this route
    //   // which is lazy-loaded when the route is visited.
    //   component: () => import('../views/AboutView.vue')
    // }
  ]
})


router.beforeEach((to, from, next) => {
  if (to.meta.requiresDatabase && !DatabaseService.selectedDatabase) {
    // Si la route nécessite une base de données et aucune n'est sélectionnée, redirigez vers la page d'accueil.
    next('/');
  } else {
    next();
  }
});


export default router
