# Installation et exécution du projet

## Prérequis

### Avoir installer Node.js, MySQL (ou autre), SQLite and MongoDB

## Se positionner dans /punto/project et installer les dépendances nécéssaires

```sh
npm install
```


## Lancer le server hébergeant l'API et le projet Vue 

### Exécuter server.js

```sh
node server.js
```


### Compiler et exécuter le projet Vue 

```sh
npm run serve
```
